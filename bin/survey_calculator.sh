#!/bin/bash
# Capstone project 5 Sprint 2020
source /usr/local/share/.credentials.sh
RESULTS="/var/www/html/survey/admin/results/"
IDs=$(mktemp)
ID2=$(mktemp)

mkdir -p ${RESULTS} &> /dev/null

mysql -u ${USERNAME} -p${PASSWORD} survey<<_EOF_>${IDs}
select id from surveys;
_EOF_


tail -n +2 $IDs |grep -v NULL > $ID2

for A in $(cat ${ID2}); do
# Prep files
TMP=$(mktemp)
RES=$(mktemp)
NAMEFILE=$(mktemp)
HEADER=$(mktemp)

mysql -u ${USERNAME} -p${PASSWORD} survey<<_EOF_>${NAMEFILE}
SELECT course_filename FROM surveys WHERE id="${A}";
_EOF_
FILENAME=$(cat $NAMEFILE|tail -1)
FILE="/dev/shm/${FILENAME}_results_${A}.csv"
FILE_ALL="/dev/shm/${FILENAME}_all_responses_${A}.csv"

# Get survey results -- ALL
mysql -u ${USERNAME} -p${PASSWORD} survey<<_EOF_>${TMP}
UPDATE ${A}_responses SET Summary = REPLACE(Summary, "\r\n", " /n ");
select Reviewee,NetID,Q01,Q02,Q03,Q04,Summary from ${A}_responses;
_EOF_

tail -n +2 $TMP |grep -v NULL > $RES

# Dump results into a flat file for manual review if necessary
mysql -u ${USERNAME} -p${PASSWORD} survey<<_EOF_>$HEADER
SELECT course_title,course_section,course_description,semester,year,instructor_name FROM surveys WHERE id="${A}";
_EOF_
tail -1 $HEADER | tr "\t" " " > ${FILE_ALL}
echo >> ${FILE_ALL}
echo 'Student Reviewed,,Reviewer,,Q1,Q2,Q3,Q4,Comments' >> ${FILE_ALL}
sed -i 's/, /,/g' $RES
cat $RES | sort -k1 | sed -e 's/\t/,/g' >> ${FILE_ALL}

# Calculate averages and add them to the main results file

mysql -u ${USERNAME} -p${PASSWORD} survey<<_EOF_>$HEADER
SELECT course_title,course_section,course_description,semester,year,instructor_name FROM surveys WHERE id="${A}";
_EOF_
tail -1 $HEADER | tr "\t" " " > ${FILE}
echo >> ${FILE}
echo 'Last Name,First Name,Number of reviews,Overall Average,Q1,Q2,Q3,Q4' >> ${FILE}

# Get survey results -- AUTOGRADE
mysql -u ${USERNAME} -p${PASSWORD} survey<<_EOF_>${TMP}
select Reviewee,Q01,Q02,Q03,Q04 from ${A}_responses;
_EOF_

tail -n +2 $TMP |grep -v NULL > $RES

sed -i 's/, /,/' $RES
sed -i 's/ /_/' $RES

STUDENTS=$(awk '{print $1}' $RES |sort| uniq | paste -s -d' ')

for I in ${STUDENTS}; do
  NUM_RESP=$(egrep "^${I}" $RES | wc -l)

  Q1sum=$(egrep "^${I}" $RES | awk '{print $2}' | paste -s -d'+' | bc)
  Q1=$(echo ${Q1sum}/${NUM_RESP} | bc -l | sed -e "s/\(\.[0-9][0-9]\).*/\1/g")

  Q2sum=$(egrep "^${I}" $RES | awk '{print $3}' | paste -s -d'+' | bc)
  Q2=$(echo ${Q2sum}/${NUM_RESP} | bc -l | sed -e "s/\(\.[0-9][0-9]\).*/\1/g")

  Q3sum=$(egrep "^${I}" $RES | awk '{print $4}' | paste -s -d'+' | bc)
  Q3=$(echo ${Q3sum}/${NUM_RESP} | bc -l | sed -e "s/\(\.[0-9][0-9]\).*/\1/g")

  Q4sum=$(egrep "^${I}" $RES | awk '{print $5}' | paste -s -d'+' | bc)
  Q4=$(echo ${Q4sum}/${NUM_RESP} | bc -l | sed -e "s/\(\.[0-9][0-9]\).*/\1/g")

  OVERALLsum=$(echo "${Q1}+${Q2}+${Q3}+${Q4}" | bc -l)
  OVERALL=$(echo "${OVERALLsum}/4" | bc -l | sed -e "s/\(\.[0-9][0-9]\).*/\1/g")

  echo ${I},${NUM_RESP},${OVERALL},${Q1},${Q2},${Q3},${Q4} >> ${FILE}
done

# Cleanup!
rm -f $TMP $RES $NAMEFILE $HEADER

# Stage files
/usr/bin/mv ${FILE} ${RESULTS}
/usr/bin/mv ${FILE_ALL} ${RESULTS}
done


rm -f $IDs $ID2
chown apache:apache -R ${RESULTS}
restorecon -FR ${RESULTS}
