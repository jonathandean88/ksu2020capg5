-- Run once only!
CREATE DATABASE survey;
USE survey;

-- Run once only!
CREATE TABLE 'surveys' (
  'id' int(20) NOT NULL,
  'course_title' varchar(80),
  'course_section' varchar(40),
  'course_description' varchar(200),
  'course_filename' varchar(200),
  'semester' varchar(10),
  'year' int(4),
  'instructor_name' varchar(50),
  'instructor_email' varchar(80),
  PRIMARY KEY ('id')
);

-- Create new user for the app
CREATE USER "surveyUser"@localhost IDENTIFIED BY "${PASSWORD}";
GRANT ALL PRIVILEGES ON survey.* TO "surveyUser"@localhost;
FLUSH PRIVILEGES;

-- The following is run once for each survey
CREATE TABLE '$SURVEY_ID_students' (
  id int(11) NOT NULL AUTO_INCREMENT,
  lastname varchar(50),
  firstname varchar(50),
  PRIMARY KEY ('id')
);

CREATE TABLE '$SURVEY_ID_responses' (
  'id' int(11) NOT NULL AUTO_INCREMENT,
  Netid varchar(50),
  Reviewee varchar(100),
  Q01 varchar(5),
  Q02 varchar(5),
  Q03 varchar(5),
  Q04 varchar(5),
  Summary text,
  PRIMARY KEY ('id')
);
