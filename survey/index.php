<!DOCTYPE html>
<html lang="en" dir="ltr" class="en dir-ltr  no-js " >

<?php
if (empty($_GET['id'])) {
  header('Location: error.php');
  exit;
}
?>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="generator" content="ksusurvey http://www.ksusurvey.org" />

        
<link rel="stylesheet" type="text/css" href="assets/26e99903/noto.css" />
<link rel="stylesheet" type="text/css" href="assets/b66cfa6f/css/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="assets/aa8a5c94/survey.css" />
<link rel="stylesheet" type="text/css" href="assets/9de01f56/template-core.css" />
<link rel="stylesheet" type="text/css" href="assets/9de01f56/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" />
<link rel="stylesheet" type="text/css" href="assets/ef5e15e2/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="assets/ef5e15e2/yiistrap.min.css" />
<link rel="stylesheet" type="text/css" href="assets/13d09538/css/ajaxify.css" />
<link rel="stylesheet" type="text/css" href="assets/13d09538/css/theme.css" />
<link rel="stylesheet" type="text/css" href="assets/13d09538/css/custom.css" />
<link rel="stylesheet" href="css/style.css">
<script type='text/javascript'>window.debugState = {frontend : (0 === 1), backend : (0 === 1)};</script><script type="text/javascript" src="assets/768a64bb/jquery-3.4.1.min.js" class="headScriptTag"></script>
<script type="text/javascript" src="assets/768a64bb/jquery-migrate-3.1.0.min.js" class="headScriptTag"></script>
<script type="text/javascript" src="assets/b75211dc/build/lslog.js" class="headScriptTag"></script>
<script type="text/javascript" src="assets/b1eda464/pjax.js" class="headScriptTag"></script>
<script type="text/javascript" src="assets/e4e1d223/moment-with-locales.min.js" class="headScriptTag"></script>
<script type="text/javascript" src="assets/aa8a5c94/survey.js" class="headScriptTag"></script>
<script type="text/javascript" src="assets/9de01f56/template-core.js" class="headScriptTag"></script>
<script type="text/javascript" src="assets/ef5e15e2/bootstrap.min.js" class="headScriptTag"></script>
<script type="text/javascript" src="assets/ef5e15e2/plugins/bootstrapconfirm/bootstrapconfirm.min.js" class="headScriptTag"></script>
<script type="text/javascript" src="assets/13d09538/scripts/theme.js" class="headScriptTag"></script>
<script type="text/javascript" src="assets/13d09538/scripts/ajaxify.js" class="headScriptTag"></script>
<script type="text/javascript" src="assets/13d09538/scripts/custom.js" class="headScriptTag"></script>
<script type="text/javascript" src="assets/3be36ea6/survey_runtime.js" class="headScriptTag"></script>
<script type="text/javascript" src="assets/776f9056/em_javascript.js" class="headScriptTag"></script>
<style>
</style>
<?PHP
try {
  require('dbconnect.php');
  require('questions.php');
}

// ERROR
catch (Exception $ex) {
   die("Failed to connect to database");
}
  $survey = $_GET['id'];
  ?>

  <?php
  $stmt1 = $conn->prepare("SELECT * FROM surveys WHERE id=$survey");
  $stmt1->execute();
  $info1 = $stmt1->fetchAll();
  if (empty($info1)) {
    header('Location: error.php');
    exit;
  }
  ?>
  
<title>
  <?php foreach($info1 as $info): ?>
   <?= $info['course_title']; ?>
  <?php endforeach; ?>
</title>

    <link rel="shortcut icon" href="favicon.ico" />
    <script type="text/javascript">
        if(window.basicThemeScripts === undefined){ 
            window.basicThemeScripts = new ThemeScripts(); 
        } 
    </script>
    
</head>

<body style="padding-top: 25px;" class=" vanilla font-noto lang-en  "  >

 <nav id="peer" class="navbar navbar-dark bg-dark sticky-top">
        <div class="peer-image">
            <a class="navbar-brand" href="#">
                <img src="images/ksulogo3.png" width="200" height="50" id="peer-image" class="d-inline-block align-top" alt="">
            </a>
        </div>


</nav>

</div>


                            <div id="beginScripts" class="script-container">
                <script type="text/javascript" src="assets/3ec9fa6a/decimal.js"></script>
<script type="text/javascript" src="assets/3f4ee358/decimalcustom.js"></script>
<script type="text/javascript" src="assets/3be36ea6/nojs.js"></script>
<script type="text/javascript">
/*<![CDATA[*/
setJsVar();
$.extend(LSvar.lang,{ "yes":"Yes","no":"No" })
var LEMmode='survey';
var LEMgseq='';
ExprMgr_process_relevance_and_tailoring = function(evt_type,sgqa,type){
if (typeof LEM_initialized == 'undefined') {
LEM_initialized=true;
LEMsetTabIndexes();
}
if (evt_type == 'onchange' && (typeof last_sgqa !== 'undefined' && sgqa==last_sgqa) && (typeof last_evt_type !== 'undefined' && last_evt_type == 'TAB' && type != 'checkbox')) {
  last_evt_type='onchange';
  last_sgqa=sgqa;
  return;
}
last_evt_type = evt_type;
last_sgqa=sgqa;
  relChange7=false;
  relChange8=false;
  relChange9=false;
  relChange12=false;
  relChange7=false;
  relChange8=false;
  relChange9=false;
  relChange12=false;
  relChangeG0=false;

}

/*]]>*/
</script>

            </div>
               

                <article>

            <div id="dynamicReloadContainer">       
                <!-- Outer Frame Container -->
<div class=" outerframe    container  " id="outerframeContainer"  >
        
<!-- Main Row -->
<div id="main-row"  >
    <!-- Main Col -->
    <div class="  col-centered  space-col" id="main-col" >

<!-- Start of the main Form-->
<form id="ksusurvey" name="ksusurvey" autocomplete="off" class="survey-form-container form" action="survey.php?id=<?php echo $survey ?>" method="post">
<input type="hidden" value="NFF6bTV0UUZrcHBmMnpla0lJcUtzaVNUbEN2U1EzbVmq65Zjld8P7l-FUul34kQJ5XP1mi1s29LhO3ZMU_VkHg==" name="YII_CSRF_TOKEN" />

<!-- Field Names -->
<input type='hidden' name='fieldnames' value='Reviewee|Netid|Q01|Q02|Q03|Q04|Summary' id='fieldnames' />

<input type='hidden' name='thisstep' value='2' id='thisstep' />
<input type='hidden' name='sid' value='284989' id='sid' />
<input type='hidden' name='start_time' value='1583081471' id='start_time' />
<input type='hidden' name='LEMpostKey' value='770584257' id='LEMpostKey' />


<!-- Submit button -->
<button type="submit" id="defaultbtn" value="default" name="move" class="submit hidden" style="display:none">default</button>
 <!-- main form -->

        <!-- emScriptsAndHiddenInputs updated -->
<input type='hidden' id='relevance7' name='relevance7' value='1'/>
<input type='hidden' id='relevance8' name='relevance8' value='1'/>
<input type='hidden' id='relevance9' name='relevance9' value='1'/>
<input type='hidden' id='relevance12' name='relevance12' value='1'/>
<input type='hidden' id='relevanceG0' name='relevanceG0' value='1'/>
<input type='hidden' id='aQuestionsWithDependencies' data-qids='[]' />

        

<!-- No JavaScript alert -->
<div class=' ls-js-hidden warningjs  alert alert-danger '    data-type='checkjavascript'>
    Caution: JavaScript execution is disabled in your browser or for this website. You may not be able to answer all questions in this survey. Please, verify your browser parameters.
</div>


        
            
<!-- Welcome Message -->
<div id="welcome-container" class="" >
    <div class="col-sm">
            <div class="container-fluid peer-review">          

    </div> 

</div>
    <!-- Survey Name -->
    <h1 class=" survey-name  text-center"  >
      <?php foreach($info1 as $info): ?>
       <?= $info['course_title']; ?> Peer Review
      <?php endforeach; ?>
    </h1>
    <br>
    <!-- Survey description -->
    <div class=" survey-description  text-info text-center" >
      <?php foreach($info1 as $info): ?>
       <?= $info['course_description']; ?>
      <?php endforeach; ?>
    </div>
    <br><br>

    <!-- Welcome text -->
    <div class=" survey-welcome  h4 text-primary" >
      <?php foreach($info1 as $info): ?>
        This is a review of your peers in <?= $info['course_title']; ?>, section <?= $info['course_section']; ?> in the <?= $info['semester']; ?> semester, <?= $info['year']; ?>.
      <?php endforeach; ?>
    </div>

    <!-- Question count -->
    <div class=" number-of-questions   text-muted" >
        <div class=' question-count-text ' >
      <?php foreach($info1 as $info): ?>
       <b>NOTE</b>: If the above information does not look correct or you have any questions regarding the evaluation, please contact your instructor <b><?= $info['instructor_name']; ?></b> on D2L or at "<?= $info['instructor_email']; ?>".
      <?php endforeach; ?>
    <br><br>There are 4 questions in this survey.
                                  </div>
                                  
  </div>
</div>
<br>
<hr align="center" width="75%">

<!-- START OF GROUP: Survey   -->
<div id='group-0' class=" group-outer-container  space-col" >
        
<div class=" group-container  space-col" >
 
<!-- Group Name -->
    <div class=" group-title   text-center h2 space-col" >
        Survey
    </div>
    
    <!-- PRESENT THE QUESTIONS -->
             <!-- Question 8  -->
<div  id="question8" class="row text-short mandatory   question-container  " >
    
    <!-- Question Q0002 -->

        
<!-- Question text -->
<div class=" question-title-container   col-xs-12 "  >
<div class=" question-text " >
    <div id="ls-question-text-Netid" class=" ls-label-question " >
        <h4><?php echo $qReviewer ?></h4>
    </div>
</div>

    
</div>
      
<!-- ksusurvey valid message and help -->
<div class=" question-valid-container   text-info col-xs-12" >
               </div>


        
<!-- Answer -->
<div class=" answer-container    col-xs-12" >
        
<!-- answer -->
<div class='ls-answers answer-item text-item '>
            <!-- Prefix -->
        
        <!-- Input -->
<select id="answerNetid" name="Netid">
  <option disabled selected value> -- select an option -- </option>

  <?php
  $stmt = $conn->prepare("SELECT lastname, firstname FROM ${survey}_students");
  $stmt->execute();
  $users = $stmt->fetchAll();
  ?>

  <?php foreach($users as $user): ?>
    <option value="<?= $user['lastname']; ?>, <?= $user['firstname']; ?>"><?= $user['lastname']; ?>, <?= $user['firstname']; ?></option>
  <?php endforeach; ?>

</select>

        <!-- Suffix -->
            </div>

<!-- end of answer -->
</div>


        
<!-- Survey question help -->
<div class=" question-help-container   text-info col-xs-12 " >
                
    </div>


    <!-- End of question Q0002 -->

</div>
<!-- End of question  8  -->

	
             <!-- Question 7  -->
<div  id="question7" class="row text-short mandatory   question-container  " >
    


    <!-- Question Q00001 -->

        
<!-- Question text -->
<div class=" question-title-container   col-xs-12 "  >
<div class=" question-text " >
    <div id="ls-question-text-Reviewee" class=" ls-label-question " >
        <h4><?php echo $qReviewee ?></h4>
    </div>
</div>

    
</div>

<div class=" question-valid-container   text-info col-xs-12" >
               </div>

<!-- Answer -->
<div class=" answer-container    col-xs-12" >
        
<!-- answer -->
<div class='ls-answers answer-item text-item '>
            <!-- Prefix -->
        
        <!-- Input -->
<!--        <input
            class="form-control "
            type="text"
            name="Reviewee"
            id="answerReviewee"
            value=""
                                    aria-labelledby="ls-question-text-Reviewee"
	/> 
-->


<select id="answerReviewee" name="Reviewee">
  <option disabled selected value> -- select an option -- </option>
  <?php foreach($users as $user): ?>
    <option value="<?= $user['lastname']; ?>, <?= $user['firstname']; ?>"><?= $user['lastname']; ?>, <?= $user['firstname']; ?></option>
  <?php endforeach; ?>
</select>

        <!-- Suffix -->
            </div>

<!-- end of answer -->
</div>
       
<!-- Survey question help -->
<div class=" question-help-container   text-info col-xs-12 " >
                
    </div>

    <!-- End of question Q00001 -->

</div>
<!-- End of question  7  -->


             <!-- Question 9  -->
<div  id="question9" class="row array-5-pt mandatory   question-container  " >
    


    <!-- Question Q00003 -->

        
<!-- Question text -->
<div class=" question-title-container   col-xs-12 "  >

<div class=" question-text " >
    <div id="ls-question-text-284989X2X9" class=" ls-label-question " >
        <h4><?php echo $qScale ?></h4>
    </div>
</div>

    
</div>



        
<!-- ksusurvey valid message and help -->
<div class=" question-valid-container   text-info col-xs-12" >
               </div>


        
<!-- Answer -->
<div class=" answer-container    col-xs-12" >
        <!-- Array 5 point choice -->

<!-- answer -->
<table class="ls-answers subquestion-list questions-list radio-array table table-bordered table-hover table-5-point-array" role="group" aria-labelledby="ls-question-text-284989X2X9">
    <!-- Columns -->
    <colgroup class="col-responses">
        <col class="col-answers" style='width: 33%;' />
        
                        
<!-- col.twig -->
<col class="" style='width: 13.4%;' />
<!-- end of col.twig -->

<!-- col.twig -->
<col class="" style='width: 13.4%;' />
<!-- end of col.twig -->

<!-- col.twig -->
<col class="" style='width: 13.4%;' />
<!-- end of col.twig -->

<!-- col.twig -->
<col class="" style='width: 13.4%;' />
<!-- end of col.twig -->

<!-- col.twig -->
<col class="" style='width: 13.4%;' />
<!-- end of col.twig -->

        
    </colgroup>
    <!-- Table headers -->
    <thead aria-hidden="true">
        <tr class="ls-heading"><!-- same class for repeat heading too -->
            
                                

<!-- thead -->
<td class=""></td>
<!-- end of thead -->


<!-- thead -->
<th class="answer-text">
    1
</th>
<!-- end of thead -->


<!-- thead -->
<th class="answer-text">
    2
</th>
<!-- end of thead -->


<!-- thead -->
<th class="answer-text">
    3
</th>
<!-- end of thead -->


<!-- thead -->
<th class="answer-text">
    4
</th>
<!-- end of thead -->


<!-- thead -->
<th class="answer-text">
    5
</th>
<!-- end of thead -->


            
        </tr>
    </thead>
    <!-- Table Body -->
    <tbody>
        
                        
<!-- answer_row -->
<tr id="javatbdQ01" class="answers-list radio-list form-group ls-even"   role="radiogroup"  aria-labelledby="answertextQ01">
    <th id="answertextQ01" class="answertext control-label}">
       <?php echo $qQ1 ?> 

                <input id="javaQ01" disabled="disabled" type="hidden" value="" name="javaQ01" />
    </th>
                    
<!-- td_input -->
<td class="answer_cell_1 answer-item radio-item">
    <input
        type="radio"
        name="Q01"
        id="answerQ01-1"
        value="1"
        
     />
    <label for="answerQ01-1" class="ls-label-xs-visibility">
        Not Pleased
    </label>
</td>
<!-- end of td_input -->

<!-- td_input -->
<td class="answer_cell_2 answer-item radio-item">
    <input
        type="radio"
        name="Q01"
        id="answerQ01-2"
        value="2"
        
     />
    <label for="answerQ01-2" class="ls-label-xs-visibility">
        Slightly Pleased
    </label>
</td>
<!-- end of td_input -->

<!-- td_input -->
<td class="answer_cell_3 answer-item radio-item">
    <input
        type="radio"
        name="Q01"
        id="answerQ01-3"
        value="3"
        
     />
    <label for="answerQ01-3" class="ls-label-xs-visibility">
        Pleased
    </label>
</td>
<!-- end of td_input -->

<!-- td_input -->
<td class="answer_cell_4 answer-item radio-item">
    <input
        type="radio"
        name="Q01"
        id="answerQ01-4"
        value="4"
        
     />
    <label for="answerQ01-4" class="ls-label-xs-visibility">
        Very Pleased
    </label>
</td>
<!-- end of td_input -->

<!-- td_input -->
<td class="answer_cell_5 answer-item radio-item">
    <input
        type="radio"
        name="Q01"
        id="answerQ01-5"
        value="5"
        
     />
    <label for="answerQ01-5" class="ls-label-xs-visibility">
        Strongly Pleased
    </label>
</td>
<!-- end of td_input -->


</tr>
<!-- end of answer_row -->

<!-- answer_row -->
<tr id="javatbdQ02" class="answers-list radio-list form-group ls-odd"   role="radiogroup"  aria-labelledby="answertextQ02">
    <th id="answertextQ02" class="answertext control-label}">
        <?php echo $qQ2 ?>

                <input id="javaQ02" disabled="disabled" type="hidden" value="" name="javaQ02" />
    </th>
                    
<!-- td_input -->
<td class="answer_cell_1 answer-item radio-item">
    <input
        type="radio"
        name="Q02"
        id="answerQ02-1"
        value="1"
        
     />
    <label for="answerQ02-1" class="ls-label-xs-visibility">
        Not Active
    </label>
</td>
<!-- end of td_input -->

<!-- td_input -->
<td class="answer_cell_2 answer-item radio-item">
    <input
        type="radio"
        name="Q02"
        id="answerQ02-2"
        value="2"
        
     />
    <label for="answerQ02-2" class="ls-label-xs-visibility">
        Rarely Active
    </label>
</td>
<!-- end of td_input -->

<!-- td_input -->
<td class="answer_cell_3 answer-item radio-item">
    <input
        type="radio"
        name="Q02"
        id="answerQ02-3"
        value="3"
        
     />
    <label for="answerQ02-3" class="ls-label-xs-visibility">
        Active
    </label>
</td>
<!-- end of td_input -->

<!-- td_input -->
<td class="answer_cell_4 answer-item radio-item">
    <input
        type="radio"
        name="Q02"
        id="answerQ02-4"
        value="4"
        
     />
    <label for="answerQ02-4" class="ls-label-xs-visibility">
        Frequently Active
    </label>
</td>
<!-- end of td_input -->

<!-- td_input -->
<td class="answer_cell_5 answer-item radio-item">
    <input
        type="radio"
        name="Q02"
        id="answerQ02-5"
        value="5"
        
     />
    <label for="answerQ02-5" class="ls-label-xs-visibility">
        Very Active
    </label>
</td>
<!-- end of td_input -->


</tr>
<!-- end of answer_row -->

<!-- answer_row -->
<tr id="javatbdQ03" class="answers-list radio-list form-group ls-even"   role="radiogroup"  aria-labelledby="answertextQ03">
    <th id="answertextQ03" class="answertext control-label}">
        <?php echo $qQ3 ?>

                <input id="javaQ03" disabled="disabled" type="hidden" value="" name="javaQ03" />
    </th>
                    
<!-- td_input -->
<td class="answer_cell_1 answer-item radio-item">
    <input
        type="radio"
        name="Q03"
        id="answerQ03-1"
        value="1"
        
     />
    <label for="answerQ03-1" class="ls-label-xs-visibility">
        Not Involved
    </label>
</td>
<!-- end of td_input -->

<!-- td_input -->
<td class="answer_cell_2 answer-item radio-item">
    <input
        type="radio"
        name="Q03"
        id="answerQ03-2"
        value="2"
        
     />
    <label for="answerQ03-2" class="ls-label-xs-visibility">
        Rarely Involved
    </label>
</td>
<!-- end of td_input -->

<!-- td_input -->
<td class="answer_cell_3 answer-item radio-item">
    <input
        type="radio"
        name="Q03"
        id="answerQ03-3"
        value="3"
        
     />
    <label for="answerQ03-3" class="ls-label-xs-visibility">
        Involved
    </label>
</td>
<!-- end of td_input -->

<!-- td_input -->
<td class="answer_cell_4 answer-item radio-item">
    <input
        type="radio"
        name="Q03"
        id="answerQ03-4"
        value="4"
        
     />
    <label for="answerQ03-4" class="ls-label-xs-visibility">
        Very Involved
    </label>
</td>
<!-- end of td_input -->

<!-- td_input -->
<td class="answer_cell_5 answer-item radio-item">
    <input
        type="radio"
        name="Q03"
        id="answerQ03-5"
        value="5"
        
     />
    <label for="answerQ03-5" class="ls-label-xs-visibility">
        Constantly Involved
    </label>
</td>
<!-- end of td_input -->


</tr>
<!-- end of answer_row -->

<!-- answer_row -->
<tr id="javatbdQ04" class="answers-list radio-list form-group ls-odd"   role="radiogroup"  aria-labelledby="answertextQ04">
    <th id="answertextQ04" class="answertext control-label}">
        <?php echo $qQ4 ?>

                <input id="javaQ04" disabled="disabled" type="hidden" value="" name="javaQ04" />
    </th>
                    
<!-- td_input -->
<td class="answer_cell_1 answer-item radio-item">
    <input
        type="radio"
        name="Q04"
        id="answerQ04-1"
        value="1"
        
     />
    <label for="answerQ04-1" class="ls-label-xs-visibility">
        Unsatisfactory Performance
    </label>
</td>
<!-- end of td_input -->

<!-- td_input -->
<td class="answer_cell_2 answer-item radio-item">
    <input
        type="radio"
        name="Q04"
        id="answerQ04-2"
        value="2"
        
     />
    <label for="answerQ04-2" class="ls-label-xs-visibility">
        Need Improvement
    </label>
</td>
<!-- end of td_input -->

<!-- td_input -->
<td class="answer_cell_3 answer-item radio-item">
    <input
        type="radio"
        name="Q04"
        id="answerQ04-3"
        value="3"
        
     />
    <label for="answerQ04-3" class="ls-label-xs-visibility">
        Meets Expectation
    </label>
</td>
<!-- end of td_input -->

<!-- td_input -->
<td class="answer_cell_4 answer-item radio-item">
    <input
        type="radio"
        name="Q04"
        id="answerQ04-4"
        value="4"
        
     />
    <label for="answerQ04-4" class="ls-label-xs-visibility">
        Exceeds Expectation
    </label>
</td>
<!-- end of td_input -->

<!-- td_input -->
<td class="answer_cell_5 answer-item radio-item">
    <input
        type="radio"
        name="Q04"
        id="answerQ04-5"
        value="5"
        
     />
    <label for="answerQ04-5" class="ls-label-xs-visibility">
        Exceptional Performance
    </label>
</td>
<!-- end of td_input -->


</tr>
<!-- end of answer_row -->

        
    </tbody>
</table>

<!-- end of answer -->

</div>


        
<!-- Survey question help -->
<div class=" question-help-container   text-info col-xs-12 " >
                
    </div>


    <!-- End of question Q00003 -->

</div>
<!-- End of question  9  -->

             <!-- Question 12  -->
<div  id="question12" class="row text-long mandatory   question-container  " >
    


    <!-- Question Q00006 -->

        
<!-- Question text -->
<div class=" question-title-container   col-xs-12 "  >

<div class=" question-text " >
    <div id="ls-question-text-Summary" class=" ls-label-question " >
        <h4><?php echo $qComments ?></h4>
    </div>
</div>
    
</div>
        
<!-- ksusurvey valid message and help -->
<div class=" question-valid-container   text-info col-xs-12" >
               </div>


        
<!-- Answer -->
<div class=" answer-container    col-xs-12" >
        <!-- Long Free Text -->

<!-- answer -->
<div class='ls-answers answer-item text-item '>
    <textarea
        class="form-control "
        name="Summary"
        id="answerSummary"
        rows="5"
                        aria-labelledby="ls-question-text-Summary"
    ></textarea>
</div>

<!-- end of answer -->

</div>


        
<!-- Survey question help -->
<div class=" question-help-container   text-info col-xs-12 " >
                
    </div>


    <!-- End of question Q00006 -->

</div>
<!-- End of question  12  -->

    
    <!-- Hidden inputs -->
    
    </div>


</div>
<!-- END OF GROUP: Survey   -->

    
                

<!-- PRESENT THE NAVIGATOR -->
<div class="    row navigator space-col" id="navigator-container" >

    <!-- Previous button container -->
    <div class="   col-xs-6 text-left" >

                    </div>
    <div class="   col-xs-6 text-right">

                
                                        <!-- Button submit -->
                <button  id="ls-button-submit" type="submit" value="movesubmit" name="move"  accesskey="n" class="   ls-move-btn ls-move-submit-btn action--ls-button-submit  btn btn-lg btn-warning btn-primary">
                    Submit
                </button>
                        </div>
</div>
<!-- Extra navigator part -->
    <!-- extra tools, can be shown with javascript too (just remove ls-js-hidden class -->
    <div class="row ls-js-hidden">
        <!-- Extra button container -->
        <div class="col-xs-6 clearall-saveall-wrapper">
                                <button  type="submit" value="loadall" name="loadall"  accesskey="l" class="ls-saveaction ls-loadall  action--ls-button-submit btn btn-default">Load unfinished survey</button>
<button  type="submit" value="saveall" name="saveall"  nameaccesskey="s" class='ls-saveaction ls-saveall  action--ls-button-submit btn btn-default'>
    Resume later
</button>

                                        
<div class=" form-inline">
    <div class="">
        <label class="form-group ls-js-hidden">
            <input value="confirm" name="confirm-clearall" type="checkbox" class=""><span class="control-label">Please confirm you want to clear your response?</span>
        </label>
        <button type="submit" name="clearall" value="clearall" data-confirmedby="confirm-clearall" class="ls-clearaction ls-clearall btn btn-link" title="This action need to be confirmed.">Exit and clear survey</button>
    </div>
</div>

                    </div>
         <!-- Index container -->
            </div>
</form> <!-- main form -->
  <?php
  $conn = null;
  ?>
    </div> <!-- main col -->
</div> <!-- main row -->
</div>
    </body>
</html>

