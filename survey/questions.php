<?PHP
$qReviewer = "Please select your own name (last name, first name).";
$qReviewee = "Please select the name of the individual being reviewed (last name, first name).";
$qScale = "Grade -- 5 point scale, with 5 being the highest";
$qQ1 = "Please rate your teammate's performance<br>in terms of Quality of Work";
$qQ2 = "Please rate your teammate's performance<br>in terms of Responsiveness and Communication";
$qQ3 = "Please rate your teammate's performance<br>in terms of Involvement and Responsibility";
$qQ4 = "Please rate your teammate's<br>Overall Performance in this project";
$qComments = "Please comment on the good work of your teammate or provide justification if you give your teammate a score of lower than 3.";
?>
