<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin Page</title>
    <link rel="shortcut icon" href="favicon.ico" />

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="css/form-validation.css" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
	  <style>
    table {
      font-family: arial, sans-serif;
      border-collapse: collapse;
      width: 100%;
    }
    
    td, th {
      border: 1px solid #dddddd;
      text-align: left;
      padding: 8px;
    }
    
    tr:nth-child(even) {
      background-color: #dddddd;
    }
    </style>
  </head>

  <body class="bg-light">

  <nav class="navbar navbar-expand-md navbar-light bg-light sticky-top">
        <div class="container-fluid">
            <a class="navbar-brand" href="index.php"><img src="images/ksulogo3.png" width="200" height="50" class="d-inline-block align-top" alt=""></a>
            
            <ul class="nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link active font-weight-bold" href="index.php">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="import.html">Add a New Course</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="results">Results Files</a>
                </li>
            </ul>
        </div>
    </nav>
  
<div class="container">
  
  <div class="py-2 text-center">
  <hr align="center" width="25%" color="#FDBB30">
    <h2>Survey Admin Page</h2>
    <hr align="center" width="25%" color="#FDBB30">
  </div>

      
  <?php
    try {
 require('../dbconnect.php');
}

// ERROR
catch (Exception $ex) {
   die("Failed to connect to database");
}

    $stmt = $conn->prepare("SELECT * FROM surveys");
    $stmt->execute();
    $surveys = $stmt->fetchAll();
    ?>


    
    <h2>Existing Surveys</h2>
    
    <table class="table">
      <tr>
        <th></th>
        <th>Instructor name</th>
        <th>Course Title</th>
        <th>Year</th>
        <th>Semester</th>
        <th>Course Section</th>
        <th>Course Description</th>
        <th></th>
        <th></th>
        <th></th>
      </tr>
    
    
    
    <?php foreach($surveys as $survey): ?>
      <tr>
        <td><a href="../index.php?id=<?= $survey['id']; ?>">Link</a></td>
        <td><?= $survey['instructor_name']; ?></td>
  	    <td><?= $survey['course_title']; ?></td>
	      <td><?= $survey['year']; ?></td>
	      <td><?= $survey['semester']; ?></td>
  	    <td><?= $survey['course_section']; ?></td>
	      <td><?= $survey['course_description']; ?></td>
        <td><a href="results/<?= $survey['course_filename']; ?>_results_<?= $survey['id']; ?>.csv">View Averages</a></td>
        <td><a href="results/<?= $survey['course_filename']; ?>_all_responses_<?= $survey['id']; ?>.csv">View All Responses</a></td>
	      <td><a href="delete.php?id=<?= $survey['id']; ?>">Delete this survey</a></td>
      </tr>
    <?php endforeach; ?>

    <?php
    $conn = null;
    ?>
  
    </table>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/holder/2.9.6/holder.min.js"></script>
    <script>
      // Example starter JavaScript for disabling form submissions if there are invalid fields
      (function() {
        'use strict';

        window.addEventListener('load', function() {
          // Fetch all the forms we want to apply custom Bootstrap validation styles to
          var forms = document.getElementsByClassName('needs-validation');

          // Loop over them and prevent submission
          var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
              if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
              }
              form.classList.add('was-validated');
            }, false);
          });
        }, false);
      })();
    </script>
<script>
document.querySelector('.custom-file-input').addEventListener('change',function(e){
  var fileName = document.getElementById("upexcel").files[0].name;
  var nextSibling = e.target.nextElementSibling
  nextSibling.innerText = fileName
})</script>
	</div>
  </body>
</html>
