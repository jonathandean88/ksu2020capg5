<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Confirmation Page</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="css/form-validation.css" rel="stylesheet">
</head>

<body class="bg-light">

<nav class="navbar navbar-expand-md navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="#"><img src="images/ksulogo3.png" width="200" height="50" class="d-inline-block align-top" alt=""></a>
            
            <ul class="nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link active font-weight-bold" href="index.php">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Add a New Course</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="results">Results Files</a>
                </li>
            </ul>
        </div>
    </nav>


 <div class="py-5 text-center"><h2>
 
<?php
// (0) CONFIG
// MUTE NOTICES
//error_reporting(E_ALL & ~E_NOTICE);
error_reporting(E_ALL);

// (1) FILE CHECK
// * HTML file type restriction can still miss at times
if (!isset($_FILES['upexcel']['tmp_name']) || !in_array($_FILES['upexcel']['type'], [
  'text/x-comma-separated-values', 
  'text/comma-separated-values', 
  'text/x-csv', 
  'text/csv', 
  'text/plain',
  'application/octet-stream', 
  'application/vnd.ms-excel', 
  'application/x-csv', 
  'application/csv', 
  'application/excel', 
  'application/vnd.msexcel', 
  'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
])) {

    echo "Unsupported File - ". $_FILES['upexcel']['type'] . "<br>Please Try Again"; 
    die();
}

try {
 require('../dbconnect.php');
}

// ERROR
catch (Exception $ex) {
   die("Failed to connect to database");
}

// (3) INIT PHP SPREADSHEET
require 'libraries/vendor/autoload.php';
if (pathinfo($_FILES['upexcel']['name'], PATHINFO_EXTENSION) == 'csv') {
  $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
} else {
  $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
}
$spreadsheet = $reader->load($_FILES['upexcel']['tmp_name']);

// (4) READ DATA & IMPORT
// ! NOTE ! EXCEL MUST BE IN EXACT FORMAT!

// Other Form Data
  $v_course_title = $_POST['course_title'];
  $v_course_section = $_POST['course_section'];
  $v_course_description = $_POST['course_description'];
  $v_course_filename = $_POST['course_filename'];
  $v_semester =  $_POST['semester'];
  $v_year =  $_POST['course_year'];
  $v_instructor_name = $_POST['instructor_name'];
  $v_instructor_email = $_POST['instructor_email'];

$id = time();

$trimmed = trim($v_course_filename, "\t\n\r\0\x0B");
$cleaned_filename = preg_replace('/[^A-Za-z0-9_]/u','', strip_tags($trimmed));

// Class Info
$sql0 = "INSERT INTO `surveys` (`id`, `course_title`, `course_section`, `course_description`, `course_filename`, `semester`, `year`, `instructor_name`, `instructor_email`) 
VALUES (?,?,?,?,?,?,?,?,?)";
  // Insert database
  
  try {
    $stmt0 = $conn->prepare($sql0);
    $stmt0->execute([$id,$v_course_title,$v_course_section,$v_course_description,$cleaned_filename,$v_semester,$v_year,$v_instructor_name,$v_instructor_email]);
    // $this->pdo->lastInsertId(); // If you need the last insert ID
   // echo " -> Inserted Successfully <br>";
  } catch (Exception $ex) {
    die("Failed to insert into database");
  }
  $stmt = null;
  
  // Create tables
$id_students = "${id}_students";
try {
  $sql1 = "CREATE TABLE `$id_students` (
    id int(11) NOT NULL AUTO_INCREMENT,
    lastname varchar(50),
    firstname varchar(50),
    PRIMARY KEY (`id`)
  )";
  $conn->exec($sql1);
} catch (Exception $ex) {
  die("Failed to create Survey database");
}

$id_responses = "${id}_responses";
try {
  $sql2 = "CREATE TABLE `$id_responses` (
  id int(11) NOT NULL AUTO_INCREMENT,
    Netid varchar(50),
    Reviewee varchar(100),
    Q01 varchar(5),
    Q02 varchar(5),
    Q03 varchar(5),
    Q04 varchar(5),
    Summary text,
    PRIMARY KEY (`id`)
  )";
  $conn->exec($sql2);
} catch (Exception $ex) {
  die("Failed to create Survey database");
}

//Spreadsheet data
$worksheet = $spreadsheet->getActiveSheet();
$sql = "INSERT INTO `$id_students` (`lastname`, `firstname`) VALUES (?, ?)";

$userlist = array();
foreach ($worksheet->getRowIterator() as $row) {
  // Fetch data
  $cellIterator = $row->getCellIterator();
  $cellIterator->setIterateOnlyExistingCells(false);
  $data = [];
  foreach ($cellIterator as $cell) {
    $data[] = $cell->getValue();
  }


    
  // Insert database
  // print_r($data);
  try {
    $stmt = $conn->prepare($sql);
    $stmt->execute($data);
    // $this->pdo->lastInsertId(); // If you need the last insert ID
    //echo $data[0] . " - Added <br>" . $data[1];
	array_push($userlist,$data[0] . " " . $data[1]);
  } catch (Exception $ex) {
    die("Failed to insert into database");
  }
  $stmt = null;
}

// (5) CLOSE DATABASE CONNECTION
if ($stmt !== null) { $stmt = null; }
if ($conn !== null) { $conn = null; }

echo "Course Successfully Added. <br><br>";
echo $v_course_title . " " . $v_course_section . " - " . $v_course_description . " - " . $v_course_filename . "<br>";
echo $v_semester . " " .  $v_year . "<br>";
echo $v_instructor_name . " " .  $v_instructor_email . "<br>";

    ?>

</h2> </div>
            
        <div class="pt-5 text-center">
            <h4>Students List </h4>
			<br>
<table class="table">
                  <tbody>

                    <?php foreach($userlist as $usr): ?>
                        <tr>

                            <td>
                                <?= $usr?>
                            </td>
                            
                        </tr>

                        <?php endforeach; ?>


                </tbody>
            </table>

            
        </div>


    </div>
</body>

</html>
