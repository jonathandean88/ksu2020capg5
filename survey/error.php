<!DOCTYPE html>
<html lang="en" dir="ltr" class="en dir-ltr  no-js " >


<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="generator" content="ksusurvey http://www.ksusurvey.org" />

        
<link rel="stylesheet" type="text/css" href="assets/26e99903/noto.css" />
<link rel="stylesheet" type="text/css" href="assets/b66cfa6f/css/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="assets/aa8a5c94/survey.css" />
<link rel="stylesheet" type="text/css" href="assets/9de01f56/template-core.css" />
<link rel="stylesheet" type="text/css" href="assets/9de01f56/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" />
<link rel="stylesheet" type="text/css" href="assets/ef5e15e2/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="assets/ef5e15e2/yiistrap.min.css" />
<link rel="stylesheet" type="text/css" href="assets/13d09538/css/ajaxify.css" />
<link rel="stylesheet" type="text/css" href="assets/13d09538/css/theme.css" />
<link rel="stylesheet" type="text/css" href="assets/13d09538/css/custom.css" />
<link rel="stylesheet" href="css/style.css">
<script type='text/javascript'>window.debugState = {frontend : (0 === 1), backend : (0 === 1)};</script><script type="text/javascript" src="assets/768a64bb/jquery-3.4.1.min.js" class="headScriptTag"></script>
<script type="text/javascript" src="assets/768a64bb/jquery-migrate-3.1.0.min.js" class="headScriptTag"></script>
<script type="text/javascript" src="assets/b75211dc/build/lslog.js" class="headScriptTag"></script>
<script type="text/javascript" src="assets/b1eda464/pjax.js" class="headScriptTag"></script>
<script type="text/javascript" src="assets/e4e1d223/moment-with-locales.min.js" class="headScriptTag"></script>
<script type="text/javascript" src="assets/aa8a5c94/survey.js" class="headScriptTag"></script>
<script type="text/javascript" src="assets/9de01f56/template-core.js" class="headScriptTag"></script>
<script type="text/javascript" src="assets/ef5e15e2/bootstrap.min.js" class="headScriptTag"></script>
<script type="text/javascript" src="assets/ef5e15e2/plugins/bootstrapconfirm/bootstrapconfirm.min.js" class="headScriptTag"></script>
<script type="text/javascript" src="assets/13d09538/scripts/theme.js" class="headScriptTag"></script>
<script type="text/javascript" src="assets/13d09538/scripts/ajaxify.js" class="headScriptTag"></script>
<script type="text/javascript" src="assets/13d09538/scripts/custom.js" class="headScriptTag"></script>
<script type="text/javascript" src="assets/3be36ea6/survey_runtime.js" class="headScriptTag"></script>
<script type="text/javascript" src="assets/776f9056/em_javascript.js" class="headScriptTag"></script>
<style>
</style>

<title>
  Error
</title>

    <link rel="shortcut icon" href="favicon.ico" />
    <script type="text/javascript">
        if(window.basicThemeScripts === undefined){ 
            window.basicThemeScripts = new ThemeScripts(); 
        } 
    </script>
    
</head>

<body style="padding-top: 25px;" class=" vanilla font-noto lang-en  "  >

 <nav id="peer" class="navbar navbar-dark bg-dark sticky-top">
        <div class="peer-image">
            <a class="navbar-brand" href="#">
                <img src="images/ksulogo3.png" width="200" height="50" id="peer-image" class="d-inline-block align-top" alt="">
            </a>
        </div>


</nav>

</div>


                            <div id="beginScripts" class="script-container">
                <script type="text/javascript" src="assets/3ec9fa6a/decimal.js"></script>
<script type="text/javascript" src="assets/3f4ee358/decimalcustom.js"></script>
<script type="text/javascript" src="assets/3be36ea6/nojs.js"></script>
<script type="text/javascript">
/*<![CDATA[*/
setJsVar();
$.extend(LSvar.lang,{ "yes":"Yes","no":"No" })
var LEMmode='survey';
var LEMgseq='';
ExprMgr_process_relevance_and_tailoring = function(evt_type,sgqa,type){
if (typeof LEM_initialized == 'undefined') {
LEM_initialized=true;
LEMsetTabIndexes();
}
if (evt_type == 'onchange' && (typeof last_sgqa !== 'undefined' && sgqa==last_sgqa) && (typeof last_evt_type !== 'undefined' && last_evt_type == 'TAB' && type != 'checkbox')) {
  last_evt_type='onchange';
  last_sgqa=sgqa;
  return;
}
last_evt_type = evt_type;
last_sgqa=sgqa;
  relChange7=false;
  relChange8=false;
  relChange9=false;
  relChange12=false;
  relChange7=false;
  relChange8=false;
  relChange9=false;
  relChange12=false;
  relChangeG0=false;

}

/*]]>*/
</script>

            </div>
               

                <article>

            <div id="dynamicReloadContainer">       
                <!-- Outer Frame Container -->
<div class=" outerframe    container  " id="outerframeContainer"  >
        
<!-- Main Row -->
<div id="main-row"  >
    <!-- Main Col -->
    <div class="  col-centered  space-col" id="main-col" >

<!-- MAIN -->
<h4>Error:  No valid survey ID was provided.  Please contact your instructor for assistance.</h4>
</div>
<!-- Extra navigator part -->
    <!-- extra tools, can be shown with javascript too (just remove ls-js-hidden class -->
    <div class="row ls-js-hidden">
        <!-- Extra button container -->
        <div class="col-xs-6 clearall-saveall-wrapper">
                                <button  type="submit" value="loadall" name="loadall"  accesskey="l" class="ls-saveaction ls-loadall  action--ls-button-submit btn btn-default">Load unfinished survey</button>
<button  type="submit" value="saveall" name="saveall"  nameaccesskey="s" class='ls-saveaction ls-saveall  action--ls-button-submit btn btn-default'>
    Resume later
</button>

                                        
<div class=" form-inline">
    <div class="">
        <label class="form-group ls-js-hidden">
            <input value="confirm" name="confirm-clearall" type="checkbox" class=""><span class="control-label">Please confirm you want to clear your response?</span>
        </label>
        <button type="submit" name="clearall" value="clearall" data-confirmedby="confirm-clearall" class="ls-clearaction ls-clearall btn btn-link" title="This action need to be confirmed.">Exit and clear survey</button>
    </div>
</div>

                    </div>
         <!-- Index container -->
            </div>
</form> <!-- main form -->
  <?php
  $conn = null;
  ?>
    </div> <!-- main col -->
</div> <!-- main row -->
</div>
    </body>
</html>

