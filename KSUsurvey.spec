Name:    KSUsurvey
Version: 1.0.0
Release: 3%{?dist}
Summary: Automated survey application by the 2020 Spring w01 Group 5 Capstone team

Group:   Group 5  
License: GPL
URL:     https://gitlab.com/jonathandean88/ksu2020capg5
Source0: KSUsurvey.tar.gz
%define debug_package %{nil}


#BuildRequires:  
Requires:  httpd
Requires:  php
Requires:  mariadb-server
Requires:  mariadb
Requires:  php-pdo
Requires:  php-mysqlnd
Requires:  php-xml
Requires:  php-json
Requires:  php-pecl-zip
Requires:  php-mbstring
Requires:  php-gd
#Requires:  composer

%description
Automated survey application by the 2020 Spring w01 Group 5 Capstone team

%prep
%setup -T -a0 -c

%build

%install
rm -rf %{buildroot}
install -d %{buildroot}/var/www/html
install -d %{buildroot}/usr/local/sbin
install -d %{buildroot}/usr/local/share
install -d %{buildroot}/etc/httpd/conf.d
install -d %{buildroot}/etc/cron.d
cp -r survey %{buildroot}/var/www/html/
chmod 0750 -R %{buildroot}/var/www/html/
install -m 0640 dist/survey %{buildroot}/etc/cron.d/
install -m 0640 dist/survey.conf %{buildroot}/etc/httpd/conf.d/
install -m 0700 bin/.credentials.sh %{buildroot}/usr/local/share
install -m 0700 bin/setup_survey %{buildroot}/usr/local/sbin
install -m 0700 bin/survey_calculator.sh %{buildroot}/usr/local/sbin
install -m 0700 bin/surveypass %{buildroot}/usr/local/sbin

(cd ${RPM_BUILD_ROOT}; find var/www/html/survey -type f -o -type d | grep -v dbconnect.php | sed 's/^/\"\//' | sed 's/$/\"/' > %{_builddir}/%{name}-%{version}/filelist.txt)

%files -f filelist.txt
%defattr(0640,apache,apache,0750)
%config(noreplace) %attr(0600,-,-) /var/www/html/survey/dbconnect.php
%config(noreplace) %attr(0644,root,root) /etc/cron.d/survey
/etc/httpd/conf.d/survey.conf
%config(noreplace) %attr(0700,root,root) /usr/local/share/.credentials.sh
%attr(0700,root,root) /usr/local/sbin/setup_survey
%attr(0700,root,root) /usr/local/sbin/survey_calculator.sh
%attr(0700,root,root) /usr/local/sbin/surveypass

%post

%clean
rm -rf $RPM_BUILD_ROOT

%changelog
* Mon Apr 27 2020 Jonathan Dean <jdean88@students.kennesaw.edu> 1.0.0-2
- FinalRPM release

* Fri Apr 17 2020 Jonathan Dean <jdean88@students.kennesaw.edu> 0.0.1-1
- Initial RPM release
